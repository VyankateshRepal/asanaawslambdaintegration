package com.amazonaws.lambda.demo;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.MimeBodyPart;

import org.apache.commons.codec.Charsets;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.InitiateMultipartUploadRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.google.common.io.Files;
import com.sun.mail.util.BASE64DecoderStream;

public class LambdaFunctionHandler implements RequestHandler<Object, String> {
	private static String saveDirectory;

	@Override
	public String handleRequest(Object input, Context context) {
		context.getLogger().log("Input: " + input);
		main(null);
		// TODO: implement your handler
		return "please check log!";
	}

	public static JSONObject check(String host, String storeType, String user, String password) {
		JSONObject jsonOutput = new JSONObject();
		try {

			// create properties field
			Properties properties = new Properties();

			properties.put("mail.pop3.host", host);
			properties.put("mail.pop3.port", "110");
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			// create the POP3 store object and connect with the pop server
			javax.mail.Store store = emailSession.getStore("pop3s");

			store.connect(host, user, password);

			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			javax.mail.Message[] messages = emailFolder.getMessages();
			System.out.println("messages.length---" + messages.length);
			if (messages.length > 0) {
				for (int i = 0, n = messages.length; i < n; i++) {
					javax.mail.Message message = messages[i];

					String contentType = message.getContentType();
					String messageContent = "";

					// store attachment file name, separated by comma
					String attachFiles = "";

					if (contentType.contains("multipart")) {
						// content may contain attachments
						Multipart multiPart = (Multipart) message.getContent();
						int numberOfParts = multiPart.getCount();
						for (int partCount = 0; partCount < numberOfParts; partCount++) {
							MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
							if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
								// this part is attachment
								part.getContent().toString();
								part.getContentType();
								if(part.getContent().toString().contains("com.sun.mail.util.BASE64DecoderStream@")){
			            			String contentUpdate = part.getContent().toString();
			            			System.out.println(contentUpdate);
								}
								
								InputStream stream = part.getInputStream();
								  BufferedReader br = new BufferedReader(new InputStreamReader(stream));

								  while (br.ready()) {
								  System.out.println(br.readLine());
								  }
								
								String fileName = part.getFileName();
								attachFiles += fileName + ", ";
								part.saveFile(saveDirectory + File.separator + fileName);
								///AmazonS3Client s3Client = (AmazonS3Client)AmazonS3Client.builder().withRegion("ap-south-1").withForceGlobalBucketAccessEnabled(true).build();
								BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAJMPAC7PQWNCMLQCQ", "MWIyqbF9lpnGrZCeYW2xYwqdFPgKmrJSwhnboVvz");
								AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
										.withCredentials(new InstanceProfileCredentialsProvider())
										.withRegion("ap-south-1")
					                    .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
					                    .enablePathStyleAccess()
					                    .disableChunkedEncoding()
					                    .build();
								
								/*PutObjectRequest request = new PutObjectRequest("testbucketupdate", "testFile", new File("D:\\Attachment\\LICENSE"));
                                ObjectMetadata metadata = new ObjectMetadata();
                                metadata.setContentType("plain/text");
                                metadata.addUserMetadata("x-amz-meta-title", "someTitle");
                                request.setMetadata(metadata);
                                s3Client.putObject(request);
                                s3Client.putObject(new PutObjectRequest("testbucketupdate", "testFile", new File("D:\\Tomcat-8.39\\LICENSE")).withCannedAcl(CannedAccessControlList.PublicRead));*/
                                
								File testFile = new File("https://drive.google.com/open?id=101ASVNBkJ9m8-Uj57ExXAv9J1B1UCfxI"); 
								///String content = IOUtils.toString(new FileReader(part.getContent().toString()));
									///	Files.toString(new File("D:\\Tomcat-8.39\\LICENSE"), Charsets.UTF_8);
								String content = IOUtils.toString((InputStream) part.getContent());
								///IOUtils.toString(new FileReader("file.txt"));
								byte[] md5 = DigestUtils.md5(content.getBytes());
								    InputStream is = new ByteArrayInputStream(content.getBytes());
								    ObjectMetadata metadata = new ObjectMetadata();
								    metadata.setContentLength(content.getBytes().length);
								    //setting max-age to 15 days
								    metadata.setCacheControl("max-age=1296000");
								    metadata.setContentMD5(new String(Base64.encodeBase64(md5)));
								    PutObjectRequest request = new PutObjectRequest("testbucketupdate", part.getFileName(),is, metadata);
								    request.setCannedAcl(CannedAccessControlList.PublicRead);
								    PutObjectResult result = s3Client.putObject(request);
							} else {
								// this part may be the message content
								messageContent = part.getContent().toString();
							}
						}

						if (attachFiles.length() > 1) {
							attachFiles = attachFiles.substring(0, attachFiles.length() - 2);
						}
					} else if (contentType.contains("text/plain") || contentType.contains("text/html")) {
						Object content = message.getContent();
						if (content != null) {
							messageContent = content.toString();
						}
					}
					System.out.println("---------------------------------");
					System.out.println("Email Number " + (i + 1));
					System.out.println("Subject: " + message.getSubject());
					System.out.println("From: " + message.getFrom()[0]);
					System.out.println("Text: " + message.getContent().toString());

					if (message.getSubject().toString().equalsIgnoreCase("resume")
							|| message.getSubject().toString().equalsIgnoreCase("cv")
							|| message.getSubject().toString().equalsIgnoreCase("biodata")) {

						try {
							String url = "https://app.asana.com/api/1.0/tasks";
							@SuppressWarnings("deprecation")
							HttpClient client = new DefaultHttpClient();
							HttpPost post = new HttpPost(url);
							post.setHeader("Authorization", "Bearer 0/94c97976672d47ec8c4198bed85722b1");
							// add required parameters to API
							List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
							urlParameters.add(new BasicNameValuePair("workspace", "697756242661260"));
							urlParameters.add(new BasicNameValuePair("name",
									"tasks for  " + message.getSubject() + "  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("notes",
									"define tasks related to person having mail:  " + message.getFrom()[0]));
							urlParameters.add(new BasicNameValuePair("projects", "697949741515335"));

							jsonOutput = getPostDataOutput(url, client, post, urlParameters);

						} catch (Exception e) {
							e.printStackTrace();
							String errorMessageAndClassWithMethod = getErrorContainingClassAndMethod();
							throw new Exception(errorMessageAndClassWithMethod + e.toString());
						}
						return jsonOutput;
					} else {
						jsonOutput = (JSONObject) jsonOutput.put("result", "no emails to work on");
					}
				}

			}

			// close the store and folder objects
			emailFolder.close(false);
			store.close();

		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonOutput;
	}

	public static void main(String[] args) {

		String host = "pop.secureserver.net";// change accordingly
		String mailStoreType = "pop3";
		String username = "vyankatesh.repal@zconsolutions.com";// change
																// accordingly
		String password = "vyankateshr";// change accordingly

		String saveDirectory = "D:/Attachment";

		LambdaFunctionHandler receiver = new LambdaFunctionHandler();
		receiver.setSaveDirectory(saveDirectory);

		check(host, mailStoreType, username, password);

	}

	public void setSaveDirectory(String dir) {
		this.saveDirectory = dir;
	}

	public static String getErrorContainingClassAndMethod() {
		final StackTraceElement e = Thread.currentThread().getStackTrace()[2];
		final String s = e.getClassName();
		String errorInMethod = s.substring(s.lastIndexOf('.') + 1, s.length()) + "." + e.getMethodName();
		return "Error in " + errorInMethod + " : ";
	}

	/**
	 * @param url
	 * @param client
	 * @param post
	 * @param urlParameters
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws UnsupportedOperationException
	 * @throws ParseException
	 */
	private static JSONObject getPostDataOutput(String url, HttpClient client, HttpPost post,
			List<NameValuePair> urlParameters) throws UnsupportedEncodingException, IOException,
			ClientProtocolException, UnsupportedOperationException, ParseException {
		JSONObject jsonOutput;
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + post.getEntity());
		System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		System.out.println(result.toString());
		JSONParser parser = new JSONParser();
		jsonOutput = (JSONObject) parser.parse(result.toString());
		return jsonOutput;
	}
	
	 
}
